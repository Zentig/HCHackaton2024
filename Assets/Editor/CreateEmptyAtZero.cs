﻿using UnityEngine;
using UnityEditor;

public class CreateEmptyMenu {
    [MenuItem("GameObject/Create Empty At Zero #%&n", priority = 0)]
    public static void CreateEmpty() {
        GameObject empty = new("GameObject");
        Undo.RegisterCreatedObjectUndo(empty, "Create new empty");
        Selection.objects = new Object[] {empty};
    }
}