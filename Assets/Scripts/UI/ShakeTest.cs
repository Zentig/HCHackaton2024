using PrimeTween;
using UnityEngine;

public class ShakeTest : MonoBehaviour
{
    [SerializeField] private ShakeSettings _shakeSettings;
    [SerializeField] private RectTransform _object;
    private Tween _shaker;

    public void Shake()
    {
        if (_shaker.isAlive)
        {
            return;
        }

        _shaker = Tween.ShakeLocalPosition(_object, _shakeSettings);
    }
}