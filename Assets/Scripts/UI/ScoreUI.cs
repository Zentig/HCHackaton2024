using TMPro;
using UnityEngine;

namespace UI
{
    public class ScoreUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private string _defaultText = "0";

        private void Start()
        {
            ScoreManager.Instance.OnUpdatedScore += UpdateUI;
            _scoreText.text = _defaultText;
        }
        private void OnDisable() 
        {
            ScoreManager.Instance.OnUpdatedScore -= UpdateUI;
        }
        private void UpdateUI(int currentScore)
        {
            _scoreText.text = $"{currentScore}";
        }
    }
}