﻿using UnityEngine;

namespace UI
{
    public class PinToEdge : MonoBehaviour
    {
        private enum Edge
        {
            Left,
            Right
        }

        [SerializeField] private Edge _edge;
        [SerializeField] private Vector3 _offset;
        private Camera _camera;

        private void Start()
        {
            _camera = Camera.main;
        }

        private void OnEnable()
        {
            transform.position = _edge switch
            {
                Edge.Left =>  new Vector3(_camera.orthographicSize * _camera.aspect, 0,0)+ _offset,
                Edge.Right =>  new Vector3(-_camera.orthographicSize * _camera.aspect, 0,0)+ _offset,
                _ => transform.position
            };
        }
    }
}