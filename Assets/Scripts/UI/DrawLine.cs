using UnityEngine;

namespace UI
{
    public class DrawLine : MonoBehaviour
    {
        [SerializeField] private Player _pm;
        [SerializeField] private float _maxLineLength = 2.5f;
        private Vector2 _startPoint, _endPoint;
        private LineRenderer _lineRenderer;
        private Camera _cam;

        private void Start()
        {
            _cam = Camera.main;
            _lineRenderer = GetComponent<LineRenderer>();
            if (_pm != null) _maxLineLength = _pm.MaxLineLengthImpact;
            
            DisableLine();
        }

        private void DisableLine()
        {
            _lineRenderer.positionCount = 0;
        }

        private void OnMouseDrag()
        {
            SetStartPoint();
            _endPoint = _cam.ScreenToWorldPoint(Input.mousePosition);

            if (Vector2.Distance(_startPoint, _endPoint) > _maxLineLength)
            {
                _endPoint = ClampLineLength();
            }

            RenderLine(_startPoint, _endPoint);
        }

        private Vector2 ClampLineLength()
        {
            return _startPoint + (_endPoint - _startPoint).normalized * _maxLineLength;
        }

        private void OnMouseUp()
        {
            DisableLine();
        }

        private void SetStartPoint()
        {
            _startPoint = transform.position;
        }

        private void RenderLine(Vector2 startPoint, Vector2 endPoint)
        {
            _lineRenderer.positionCount = 2;
            Vector3[] points = {startPoint, endPoint};
            _lineRenderer.SetPositions(points);
        }
    }
}