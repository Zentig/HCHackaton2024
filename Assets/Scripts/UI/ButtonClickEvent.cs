using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

namespace UI
{
    public class ButtonClickEvent : MonoBehaviour
    {
        [SerializeField] private int _score;
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private Animator _buttonAnimator;

        private void Update()
        {
            Debug.Log(Time.deltaTime);
        }   

        public void OnClickEvent()
        {
            _buttonAnimator.SetTrigger("Click");
            _score++;
            _scoreText.text = $"Score: {_score}"; //pr
        }
    }
}