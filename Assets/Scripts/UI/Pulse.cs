﻿using PrimeTween;
using UnityEngine;

namespace UI
{
    public class Pulse : MonoBehaviour
    {
        [SerializeField] private float _maxScale;
        [SerializeField] private TweenSettings _pulseSettings;
        private Tween _pulseTween;

        private void OnEnable()
        {
            if (_pulseTween.isAlive)
            {
                return;
            }

            _pulseTween = Tween.Scale(this.transform, _maxScale, _pulseSettings);
        }

        private void OnDisable()
        {
            _pulseTween.Stop();
        }
    }
}