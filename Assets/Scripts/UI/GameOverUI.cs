﻿using UnityEngine;

namespace UI
{
    public class GameOverUI : MonoBehaviour
    {
        [SerializeField] private GameObject _visuals;

        private void OnEnable()
        {
            Hide();
        }

        public void Show()
        {
            _visuals.SetActive(true);
        }

        public void Hide()
        {
            _visuals.SetActive(false);
        }
    }
}