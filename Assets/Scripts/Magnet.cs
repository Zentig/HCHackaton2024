﻿using System;
using Enemies;
using PrimeTween;
using UnityEngine;

public class Magnet : MonoBehaviour
{
    [SerializeField] private float _enableDelayInSeconds = 2f;
    [SerializeField] private Catcher _catcher;
    [SerializeField] private bool _playerSpawnMagnet = false;
    [SerializeField] private TweenSettings<Color> _playerOverMagnetTweenSettings;
    [SerializeField] private SpriteRenderer _spriteRenderer;

    private Coroutine _coroutine;
    private bool _playerLeftSpawn = true;
    private Tween _playerOverMagnetTween;

    private void OnEnable()
    {
        if (_playerSpawnMagnet)
        {
            Disable();
            _playerLeftSpawn = false;
        }
    }

    public void Disable()
    {
        this.enabled = false;
    }

    public void Enable()
    {
        Invoke(nameof(EnableMagnet), _enableDelayInSeconds);
    }

    private void EnableMagnet()
    {
        this.enabled = true;
        if (_playerSpawnMagnet)
        {
            _playerLeftSpawn = true;
        }
    }

    public void BeginEatingPlayer(Action onEaten)
    {
        if (_playerLeftSpawn)
        {
            _catcher.Eat(onEaten);
            _playerOverMagnetTween = Tween.Color(_spriteRenderer, _playerOverMagnetTweenSettings);
        }
    }

    public void StopEatingPlayer()
    {
        if (_playerOverMagnetTween.isAlive)
        {
            _playerOverMagnetTween.Stop();
            _spriteRenderer.color = Color.white;
        }

        _catcher.StopEating();
    }
}