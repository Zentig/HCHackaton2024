using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeleportAbility : MonoBehaviour
{
    [SerializeField] private float _constTimeTillNextTeleport;
    private float _currentTime;

    private void Start() 
    {
        _currentTime = 0;
    }
    private void OnTriggerEnter2D(Collider2D collision2D) 
    {
        if (collision2D.gameObject.TryGetComponent(out TeleportBlock teleportBlock) && _currentTime <= 0) 
        {
            transform.position = teleportBlock.GetTeleportPosition();
            _currentTime = _constTimeTillNextTeleport;
        }
    }
    private void Update() 
    {
        if (_currentTime > 0) 
        {
            _currentTime -= Time.deltaTime;
        }
    }   
}
