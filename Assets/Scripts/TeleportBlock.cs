using UnityEngine;

public class TeleportBlock : MonoBehaviour
{
    [SerializeField] private Transform _teleportTarget;
    [SerializeField] private Vector2 _teleportOffset;

    public Vector2 GetTeleportPosition()
    {
        return (Vector2) _teleportTarget.position + _teleportOffset;
    }
}