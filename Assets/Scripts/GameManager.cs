using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameOverUI _gameOverUI;

    private void Start()
    {
        SetupParameter();
    }

    private void SetupParameter()
    {
        Application.targetFrameRate = 144;
        QualitySettings.vSyncCount = 0;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public void GameOver()
    {
        _gameOverUI.Show();
    }

    public void NewGame()
    {
        _gameOverUI.Hide();
        SceneManager.LoadScene(0);
    }
    
}