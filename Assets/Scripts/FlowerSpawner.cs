using Cysharp.Threading.Tasks;
using UnityEngine;

public class FlowerSpawner : MonoBehaviour
{
    [SerializeField] private ScoreElement _spawnableObject;
    [SerializeField] private float _delayBetweenNextSpawn;
    private ScoreElement _lastSpawnedObj;

    private void Start() 
    {
        SpawnObject();
    }
    
    private async UniTask SpawnObjectWithDelay() 
    {
        await UniTask.Delay((int)(_delayBetweenNextSpawn*1000));
        SpawnObject();
    }

    private void SpawnObject() 
    {
        _lastSpawnedObj = Instantiate(_spawnableObject, transform.position, Quaternion.identity);
        _lastSpawnedObj.OnDestroyed += HandleDestroyedFlower;
    }

    private async void HandleDestroyedFlower()
    {
        AudioManager.Instance.PlayOneShot(FMODEvents.Instance.FlowerCollectedSFX, transform.position);
        await SpawnObjectWithDelay();
    }

    private void OnDrawGizmos() 
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 0.15f);
    }
}
