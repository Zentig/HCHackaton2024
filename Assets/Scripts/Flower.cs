using System;
using PrimeTween;
using UnityEngine;
using FMODUnity;

[RequireComponent(typeof(StudioEventEmitter))]
public class Flower : ScoreElement
{
    [SerializeField] private TweenSettings<Vector3> _idleTweenSettings;
    [SerializeField] private TweenSettings _collectTweenSettings;
    private StudioEventEmitter _emitter;

    private void Start() 
    {
        _emitter = AudioManager.Instance.InitializeEventEmitter(FMODEvents.Instance.FlowerIdleSFX, gameObject);
    }

    private void OnEnable()
    {
        Tween.Rotation(this.transform, _idleTweenSettings);
    }

    public override void OnPickedUp()
    {
        Tween.Scale(this.transform, 0f, _collectTweenSettings)
             .OnComplete(DestroyEvent);
    }

    private void DestroyEvent()
    {
        _emitter.Stop();
        OnDestroyedInvoke();
        Destroy(gameObject);
    }
}