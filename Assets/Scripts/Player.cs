using System;
using Enemies;
using PrimeTween;
using UnityEngine;
using FMOD.Studio;
using Random = UnityEngine.Random;

public class Player : MonoBehaviour
{
    [Header("Setup")]
    [SerializeField] private GameManager _gameManager;
    [field: SerializeField] public float MaxLineLengthImpact { get; private set; }
    [SerializeField] private float _power;
    [SerializeField] private TweenSettings _movementSettings;
    [SerializeField] private TweenSettings _deathTweenSettings;
    [SerializeField] private float _noMovementThreshold = 0.2f;

    private Vector2 _startPoint, _endPoint;
    private Rigidbody2D _rigidBody;
    private Camera _camera;
    private Tween _movementTween;
    private Vector3 _startingPosition;
    private bool _positionedOverMagnet = true;
    private Magnet _lastMagnet;
    private EventInstance _playerFootsteps;
    
    private void Start()
    {
        _camera = Camera.main;
        _rigidBody = GetComponent<Rigidbody2D>();
        _startingPosition = transform.position;
        _playerFootsteps = AudioManager.Instance.CreateInstance(FMODEvents.Instance.PlayerMovementSFX);
    }

    private void Update()
    {
        WarpToStartIfOutOfView();
        WarpToPreviousMagnetIfNotOnMagnet();
    }

    private void WarpToPreviousMagnetIfNotOnMagnet()
    {
        if (_rigidBody.velocity.magnitude <= _noMovementThreshold)
        {
            _playerFootsteps.stop(STOP_MODE.ALLOWFADEOUT);

            if (!_positionedOverMagnet)
            {
                ResetToPreviousStart();
            }
        }
        else 
        {
            PLAYBACK_STATE playbackState;
            _playerFootsteps.getPlaybackState(out playbackState);
            if (playbackState.Equals(PLAYBACK_STATE.STOPPED)) { _playerFootsteps.start(); }
            _playerFootsteps.setVolume(_rigidBody.velocity.magnitude / 16);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision2D)
    {
        if (collision2D.gameObject.TryGetComponent(out Enemy enemy))
        {
            ResetToPreviousStart();
        }

        if (collision2D.gameObject.TryGetComponent(out ScoreElement obj))
        {
            ScoreManager.Instance.AddScore(obj.GiveScoreAmount);
            obj.OnPickedUp();
            // _audioSrc.pitch = Random.Range(0.8f, 1.2f);
            // _audioSrc.PlayOneShot(_pickUpSound);
        }

        if (collision2D.TryGetComponent(out Magnet magnet))
        {
            if (_movementTween.isAlive)
            {
                return;
            }

            // TODO: Fix hack. Dont' playsound on game start
            if (Time.timeSinceLevelLoad >= 1f)
            {
                // _audioSrc.pitch = Random.Range(0.8f, 1.2f);
                // _audioSrc.PlayOneShot(_magnetSound);
            }

            _positionedOverMagnet = true;
            _lastMagnet = magnet;
            AttractPlayerTo(magnet);
            magnet.BeginEatingPlayer(KillPlayer);
            magnet.Disable();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out Magnet magnet))
        {
            _positionedOverMagnet = false;
            magnet.Enable();
            magnet.StopEatingPlayer();
        }
    }

    private void OnMouseOver()
    {
        SetStartPoint();
    }

    private void OnMouseUp()
    {
        _endPoint = _camera.ScreenToWorldPoint(Input.mousePosition);

        if (_rigidBody.velocity.magnitude <= _noMovementThreshold)
        {
            Vector2 direction = GetMovementDirection(_startPoint, _endPoint);
            ImpulseMove(direction, _power);
        }
    }

    public void ResetToStartingPosition()
    {
        transform.position = _startingPosition;
        _rigidBody.velocity = Vector2.zero;
    }

    private void WarpToStartIfOutOfView()
    {
        if (LeftScreen())
        {
            ResetToPreviousStart();
        }
    }

    private bool LeftScreen()
    {
        return transform.position.y >= _camera.orthographicSize ||
               transform.position.y <= -_camera.orthographicSize ||
               transform.position.x >= _camera.orthographicSize * _camera.aspect ||
               transform.position.x <= -_camera.orthographicSize * _camera.aspect;
    }

    private void ResetToPreviousStart()
    {
        transform.position = _lastMagnet.transform.position;
        _rigidBody.velocity = Vector2.zero;
    }

    private void ImpulseMove(Vector2 direction, float power)
    {
        _rigidBody.AddForce(direction * power, ForceMode2D.Impulse);
    }

    private Vector2 GetMovementDirection(Vector2 start, Vector2 end)
    {
        Vector2 direction = start - end;

        float x = Mathf.Clamp(direction.x, -MaxLineLengthImpact, MaxLineLengthImpact);
        float y = Mathf.Clamp(direction.y, -MaxLineLengthImpact, MaxLineLengthImpact);
        Vector2 force = new(x, y);
        return force;
    }

    private void SetStartPoint()
    {
        _startPoint = transform.position;
    }

    private void AttractPlayerTo(Magnet magnet)
    {
        _rigidBody.velocity = Vector2.zero;
        _movementTween = Tween.RigidbodyMovePosition(_rigidBody, magnet.transform.position, _movementSettings);
    }

    private void KillPlayer()
    {
        Tween.Scale(this.transform, Vector3.zero, _deathTweenSettings)
             .OnComplete(OnDeath());
    }

    private Action OnDeath()
    {
        // _audioSrc.PlayOneShot(_deathSound);
        return _gameManager.GameOver;
    }
}