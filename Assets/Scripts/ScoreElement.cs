using System;
using System.Collections;
using Cysharp.Threading.Tasks;
using UnityEngine;

public abstract class ScoreElement : MonoBehaviour
{
    [field: SerializeField] public int GiveScoreAmount { get; private set; }
    public abstract void OnPickedUp();
    public event Action OnDestroyed;
    protected void OnDestroyedInvoke() => OnDestroyed?.Invoke();
    protected void ClearEvent() => OnDestroyed = null;
}