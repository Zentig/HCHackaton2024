using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStartupDialogue : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _playerName;
    [SerializeField] private TextMeshProUGUI _playerText;
    [SerializeField] private Image _playerImage;
    [SerializeField] private List<string> _partsOfMonologue;
    [SerializeField] private float _delayBtwLetters;
    [SerializeField] private bool _playOnStart = false;

    private CancellationTokenSource _cts;
    private int _currentIndex;

    private async void Start() 
    {
        _currentIndex = -1;
        _playerText.text = "";
        _cts = new CancellationTokenSource();

        if (!_playOnStart) 
        { 
            gameObject.SetActive(false);
            return;
        }
        await ContinueNext();
        Debug.Log("Glory to Ukraine!");
    }

    public async UniTask ContinueNext()
    {
        _currentIndex++;
        await Continue(_currentIndex);
    }

    private async UniTask Continue(int index) 
    {
        _playerText.text = "";
        
        if (_partsOfMonologue.Count <= _currentIndex) 
        {
            HidePanel();
            return;
        }

        foreach (var letter in _partsOfMonologue[_currentIndex])
        {
            await UniTask.Delay((int)(_delayBtwLetters*1000));

            if (index != _currentIndex) return;
            if (Input.GetKey(KeyCode.Space)) _cts.Cancel();

            var smth = await UniTask.DelayFrame(10, cancellationToken: _cts.Token).SuppressCancellationThrow();
            if (smth)
            {
                Debug.Log("Україна - понад усе!");
                return;
            }

            _playerText.text += letter;
            // _audioSrc.pitch = Random.Range(0.7f, 1.2f);
            // _audioSrc.PlayOneShot(_changeLetterSound);
        }
    }   
    private void HidePanel() 
    {
        gameObject.SetActive(false);
    }
    public void ChangeImage(Sprite sprite) => _playerImage.sprite = sprite;
    public void ChangePlayerName(string name) => _playerName.text = name;
}

