﻿using System;
using PrimeTween;
using UnityEngine;

namespace Enemies
{
    public class Catcher : MonoBehaviour
    {
        [SerializeField] private GameObject _visuals;
        [SerializeField] private TweenSettings<Vector3> _eatTweenSettings;
        [SerializeField] private TweenSettings _hideTweenSettings;

        private Tween _tween;

        private void OnEnable()
        {
            _visuals.transform.transform.localScale = Vector3.zero;
        }

        public void Eat(Action onEaten)
        {
            _tween = Tween.Scale(_visuals.transform, _eatTweenSettings).OnComplete(onEaten);
        }

        public void StopEating()
        {
            _tween.Stop();
            _tween = Tween.Scale(_visuals.transform, 0f, _hideTweenSettings);
        }
    }
}