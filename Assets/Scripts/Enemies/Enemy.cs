using UnityEngine;

namespace Enemies
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] private Transform _startPoint, _endPoint;
        [SerializeField] private float _movingSpeed;
        private Transform _departedFrom;
        private Transform _departedTo;

        private void Start()
        {
            _departedTo = _endPoint;
            _departedFrom = _startPoint;
            transform.position = (_startPoint.position + _endPoint.position) / 2;
        }

        private void Update()
        {
            transform.position =
                Vector2.MoveTowards(transform.position, _departedTo.position, _movingSpeed * Time.deltaTime);

            if (transform.position == _departedTo.position)
            {
                (_departedFrom, _departedTo) = (_departedTo, _departedFrom);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(_startPoint.position, _endPoint.position);
            Gizmos.DrawSphere(_startPoint.position, 0.1f);
            Gizmos.DrawSphere(_endPoint.position, 0.1f);
        }
    }
}