using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;
using System;

public class AudioManager : MonoBehaviour 
{
    public static AudioManager Instance { get; private set; }

    [SerializeField] private List<EventInstance> eventInstances;
    [SerializeField] private List<StudioEventEmitter> eventEmitters;
    private EventInstance _ambience;

    private void Awake() 
    {
        eventInstances = new List<EventInstance>();
        eventEmitters = new List<StudioEventEmitter>();

        if (Instance != null) 
        {
            Debug.LogError("Found more than one AudioManager in the scene.");
        }
        Instance = this;
    }

    private void InitializeAmbience(EventReference eventReference) 
    {
        _ambience = CreateInstance(eventReference);
        _ambience.start();
    }

    public void SetAmbienceParameter(string parameterName, float value) 
    {
        _ambience.setParameterByName(parameterName, value);
    }

    private void Start() 
    {
        InitializeAmbience(FMODEvents.Instance.WindSFX);
    }

    public void PlayOneShot(EventReference sound, Vector3 worldPos) 
    {
        RuntimeManager.PlayOneShot(sound, worldPos);
    }

    public EventInstance CreateInstance(EventReference eventReference) 
    { 
        var eventInstance = RuntimeManager.CreateInstance(eventReference);
        eventInstances.Add(eventInstance);
        return eventInstance;
    }

    public StudioEventEmitter InitializeEventEmitter(EventReference eventReference, GameObject editorGameObject) 
    {
        StudioEventEmitter emitter = editorGameObject.GetComponent<StudioEventEmitter>();
        emitter.EventReference = eventReference;
        eventEmitters.Add(emitter);
        return emitter;
    }

    private void OnDestroy()
    {
        CleanUp();
    }

    private void CleanUp()
    {
        foreach (EventInstance eventInstance in eventInstances)
        {
            eventInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            eventInstance.release();
        }
        foreach (StudioEventEmitter emitter in eventEmitters)
        {
            emitter.Stop();
        }
    }
}