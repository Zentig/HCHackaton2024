using UnityEngine;
using FMODUnity;
using FMOD.Studio;

public class FMODEvents : MonoBehaviour 
{
    [field:Header("Ambience")]
    [field:SerializeField] public EventReference WindSFX { get; private set; }

    [field:Header("Flower SFX")]
    [field:SerializeField] public EventReference FlowerCollectedSFX { get; private set; }
    [field:SerializeField] public EventReference FlowerIdleSFX { get; private set; }

    [field:Header("Player SFX")]
    [field:SerializeField] public EventReference PlayerMovementSFX { get; private set; }

    public static FMODEvents Instance { get; private set; }

    private void Awake() 
    {
        if (Instance != null) 
        {
            Debug.LogError("Found more than one FMODEvents in the scene.");
        }
        Instance = this;
    }
}