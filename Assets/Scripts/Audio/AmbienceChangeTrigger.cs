using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbienceChangeTrigger : MonoBehaviour
{
    [SerializeField] private string _parameterName;
    [SerializeField] private float _parameterValue;

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.GetComponent<Player>() != null) 
        {
            AudioManager.Instance.SetAmbienceParameter(_parameterName, _parameterValue);
        } 
    }
}
