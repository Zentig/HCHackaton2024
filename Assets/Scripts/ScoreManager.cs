using System;
using UnityEngine;

public class ScoreManager : MonoBehaviour 
{
    public static ScoreManager Instance;
    
    /// <summary> <c>OnUpdatedScore</c> is invoked when score is updated, <c>T</c> is current score </summary>
    public event Action<int> OnUpdatedScore;  
    private int _score;

    private void Awake() 
    {
        if (Instance != null) 
        {
            Debug.LogError($"ScoreManager.Instance is already not null on singleton initialization");
        }
        Instance = this;
    }
    private void Start() 
    {
        _score = 0;
        OnUpdatedScore?.Invoke(_score);
    }
    public void AddScore(int score) 
    {
        _score += score;
        OnUpdatedScore?.Invoke(_score);
    }
    public void RemoveScore(int score) 
    {
        _score = Mathf.Max(_score -= score, 0);
        OnUpdatedScore?.Invoke(_score);
    }
}